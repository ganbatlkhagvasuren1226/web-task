# packages
import database
import mysql.connector
import pymysql
import paramiko
import re
from paramiko import SSHClient
from sshtunnel import SSHTunnelForwarder
import pandas as pd
from collections import OrderedDict

def open_connection():
    conn = mysql.connector.connect(
        host="127.0.0.1",
        port="3306",
        user="root",
        database="localhost",
    )
    return conn


def close_connection(conn):
    conn.close()


def select_query(conn, query_string):
    cur = conn.cursor()
    cur.execute(query_string)
    tmp = cur.fetchall()
    cur.close()
    return tmp
    
def execute_query(conn, query_string):
    print("QUERYYY STRING",query_string)
    cur = conn.cursor()
    try:
        cur.execute(query_string)
        conn.commit()
    except:
        conn.rollback()
        raise
    cur.close()
