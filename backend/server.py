import jwt
import os 
import datetime
from flask import Flask
from flask_cors import CORS
from flask_cors import CORS, cross_origin

from flask import request,make_response,jsonify
from flask_jwt_extended import create_access_token
from flask_jwt_extended import get_jwt_identity
from flask_jwt_extended import jwt_required
from flask_jwt_extended import JWTManager
import json
# packages(database)
import database
import re
from collections import OrderedDict


app = Flask(__name__)
CORS(app, support_credentials=True)

# app.config['CORS_HEADERS'] = 'application/json'
app.config["JWT_SECRET_KEY"] = 'super-secret'  # Change this!
jwt = JWTManager(app)

@app.route("/register",methods=["GET","POST"])
def login():
    data = request.get_json()
    email = data["email"]
    password = data["password"]
    conn = database.open_connection()
    insert_database = database.execute_query(conn,'INSERT INTO users(`mailaddress`,`password`) VALUES ("{0}","{1}")'.format(email,password))
    auth = request.authorization
    if auth and auth.password =="password":
        token = jwt.encode({'user':auth.username})
    return data["email"],data["password"]

def login_auth():
    data = request.get_json()
    email = data["email"]
    password = data["password"]
    conn = database.open_connection()
    auth = request.authorization
    if auth and auth.password =="password":
        token = jwt.encode({'user':auth.username})
    return data["email"],data["password"]

# Create flask app
@cross_origin(supports_credentials=True)
@app.route("/api/token",methods=["POST"])
# #Create Token login
def create_token():
    conn = database.open_connection()
    select_database = database.select_query(conn,'SELECT mailaddress,password FROM localhost.users')
    email = request.json.get("email", None)
    password = request.json.get("password", None)
    
    mailaddress_user = [l[0] for l in select_database]
    password_user = [i[0] for i in select_database]
    if email not in mailaddress_user and password not in password_user:
        return jsonify({"msg": "Bad username or password"}), 401

    access_token = create_access_token(identity=email)
    return jsonify(access_token=access_token,email=email)

@app.route("/api/post",methods=["POST"])

def create_post():
    conn = database.open_connection()
    data = request.get_json()
    name = data["name"]
    comments = data["comments"]
    result = database.execute_query(conn,'INSERT INTO posts(`name`,`comments`) VALUES ("{0}","{1}")'.format(name,comments))
    return "This data saved database"


@app.route("/api/getpost",methods=["GET"])

def get_posts():
    conn = database.open_connection()
    result = database.select_query(conn,'SELECT name,comments FROM localhost.posts')
    result_dict = {"user":[l[0] for l in result],"comments":[i[1] for i in result]}
    return json.dumps(result_dict)
if __name__ =="__main__":
    app.debug= True
    app.run(host='127.0.0.1', port=4000)