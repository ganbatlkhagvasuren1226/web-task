import React, { useContext, useEffect } from "react";
import { Button, Checkbox, Form, Input, Card, Alert } from "antd";
import { LockOutlined, UserOutlined, MailOutlined } from "@ant-design/icons";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

import logo from "../../logo_blue.png";
import Nav from "../Nav";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import axios from "axios";

const Register = () => {
  const [form] = Form.useForm();
  const navigate = useNavigate();
  const [isLoged, setIsLoged] = useState(false);
  // antd validate хийх хэсэг(message)
  const validateMessages = {
    required: "${label}を入れてください!",
    types: {
      email: "${label}は間違っています",
      number: "${label} is not a valid number!",
    },
    number: {
      range: "${label} must be between ${min} and ${max}",
    },
  };
  const Handlefunction = (values) => {
    // Button дарахад ажиллах хэсэг
    var postData = {
      email: values.email,
      password: values.password,
    };
    let axiosConfig = {
      headers: {
        "Content-Type": "application/json;charset=UTF-8",
        "Access-Control-Allow-Origin": "*",
      },
    };
    axios
      .post("http://127.0.0.1:4000/register", postData, axiosConfig)
      .then(function (res) {
        console.log("Hello", res.data);
        toast.success("登録が完了", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      });
  };
  return (
    <>
      <h1 className="flex justify-around mt-[10px]">登録フォーム</h1>
      <hr />
      <div className="bg-zinc-100 min-h-screen">
        <div className="flex md:bg-green py-[60px] justify-center">
          <Card
            className="shadow-sm rounded-sm"
            style={{ width: 400, height: 500 }}
            cover={<img className="opacity-100" src={logo} />}
          >
            <Form
              className="w-[320px] font-serif"
              layout="vertical"
              validateMessages={validateMessages}
              onFinish={Handlefunction}
            >
              <Form.Item
                name={"email"}
                label="メールアドレス"
                rules={[
                  {
                    type: "email",
                    required: true,
                    message: "メールを入れてください！",
                  },
                ]}
              >
                <Input
                  prefix={<MailOutlined className="site-form-item-icon" />}
                  type="email"
                ></Input>
              </Form.Item>
              <Form.Item
                name="password"
                label="パスワード"
                rules={[
                  { required: true, message: "パスワードを入れてください！" },
                ]}
              >
                <Input
                  prefix={<LockOutlined className="site-form-item-icon" />}
                  type="password"
                ></Input>
              </Form.Item>
              <Button
                type="danger"
                htmlType="submit"
                className="w-[310px] font-serif"
              >
                登録
              </Button>
            </Form>
            <Button
              danger
              type="text"
              className="mt-[-10px]"
              onClick={() => navigate("/")}
            >
              戻る
            </Button>
            {isLoged ? navigate("/blog") : ""}
          </Card>
        </div>
      </div>
    </>
  );
};
export default Register;
