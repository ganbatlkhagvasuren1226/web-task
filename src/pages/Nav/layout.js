import { React, useEffect, useState } from "react";
import {
  Text,
  Radio,
  Image,
  Avatar,
  Menu,
  Button,
  Popover,
  Tooltip,
} from "antd";
import { useNavigate, Link } from "react-router-dom";
import logo from "../../logo_white.png";
import { MenuButton } from "../../components/Button";
import { UserOutlined } from "@ant-design/icons";
import Post from "../Post";
const Layout = () => {
  const [sidebar, setSidebar] = useState(false);
  const navigate = useNavigate();
  const handlelogout = () => {
    navigate("/");
    sessionStorage.clear();
  };
  return (
    <>
      <div>
        <div className="flex justify-between items-center bg-zinc-300 ">
          <a href="/blog" className="flex items-center">
            <img src={logo} className="mr-5 h-6 sm:h-10" alt="Cheat Logo" />
          </a>
          <button
            data-collapse-toggle="navbar-default"
            type="button"
            className="inline-flex items-center p-2 ml-3 text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-600 focus:ouline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-800 dark:hover:bg-gray-700 dark:focus:ring-gray-800"
            aria-controls="navbar-default"
            aria-expanded="false"
          >
            <span className="sr-only">Open main menu</span>
          </button>
          <div
            className="flex flex-cols gap-2 hidden w-full md:block md:w-auto"
            id="navbar-default"
          >
            <Tooltip title="Admin user" placement="top">
              <Avatar
                size={34}
                style={{ backgroundColor: "#7265e6", verticalAlign: "middle" }}
              >
                {sessionStorage.getItem("name")}
              </Avatar>
            </Tooltip>
            <Button
              type="primary"
              className="flex w-[80px] items-center"
              shape="round"
              onClick={handlelogout}
            >
              Logout
            </Button>
          </div>
        </div>
        <Post />
      </div>
    </>
  );
};
export default Layout;
