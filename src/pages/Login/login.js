import React, { useContext, useEffect } from "react";
import { Button, Checkbox, Form, Input, Card, Alert } from "antd";
import { LockOutlined, UserOutlined, MailOutlined } from "@ant-design/icons";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

import logo from "../../logo_blue.png";
import Nav from "../Nav";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const Login = () => {
  const token = localStorage.getItem("token");
  const navigate = useNavigate();
  const [isLoged, setIsLoged] = useState(false);
  // antd validate хийх хэсэг(message)
  const validateMessages = {
    required: "${label}を入れてください!",
    types: {
      email: "${label}は間違っています",
    },
  };
  // Button дарахад ажиллах хэсэг
  const Handlefunction = (values) => {
    const opts = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
      },
      // mode: "no-cors",
      body: JSON.stringify({
        email: values.email,
        password: values.password,
      }),
    };

    fetch("http://127.0.0.1:4000/api/token", opts)
      .then((resp) => {
        if (resp.status === 200) {
          sessionStorage.setItem("name", values.email);
          setTimeout(setIsLoged(true), 5000);
          return toast.success(
            `あなたは${sessionStorage.getItem("name")}でアクセスしました`,
            {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            }
          );
        } else {
          toast.warn("メールアドレスとパスワードを正しく入力されていません。", {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        }
      })
      .then((data) => {
        console.log("this is came from the backend", data);
      });
  };

  return (
    <>
      <div className="bg-zinc-100 min-h-screen">
        <div className="flex md:bg-green py-[60px] justify-center">
          <Card
            className="shadow-sm rounded-sm"
            style={{ width: 400, height: 500 }}
            cover={<img className="opacity-100" src={logo} />}
          >
            <Form
              className="w-[320px] font-serif"
              layout="vertical"
              validateMessages={validateMessages}
              onFinish={Handlefunction}
            >
              <Form.Item
                name={"email"}
                label="メールアドレス"
                rules={[
                  {
                    type: "email",
                    required: true,
                    message: "メールを入れてください！",
                  },
                ]}
              >
                <Input
                  prefix={<MailOutlined className="site-form-item-icon" />}
                  type="email"
                ></Input>
              </Form.Item>
              <Form.Item
                name="password"
                label="パスワード"
                rules={[
                  { required: true, message: "パスワードを入れてください！" },
                ]}
              >
                <Input
                  prefix={<LockOutlined className="site-form-item-icon" />}
                  type="password"
                ></Input>
              </Form.Item>
              <Button
                type="link"
                className="mt-[-10px] ml-[190px]"
                onClick={() => navigate("/register")}
              >
                パスワード忘れ
              </Button>
              <Button
                type="danger"
                htmlType="submit"
                className="w-[310px] font-serif"
              >
                ログイン
              </Button>
            </Form>
            {isLoged ? navigate("/blog") : ""}
          </Card>
        </div>
      </div>
    </>
  );
};

export default Login;
