/* eslint-disable no-undef */
import React, {
  useContext,
  useEffect,
  useCallback,
  createElement,
} from "react";

import { useState } from "react";
import { useNavigate, Link } from "react-router-dom";
import {
  DislikeFilled,
  DislikeOutlined,
  LikeFilled,
  LikeOutlined,
} from "@ant-design/icons";
import moment from "moment";
import { Avatar, Comment, Tooltip } from "antd";

const Posted = (props) => {
  console.log(props);
  const navigate = useNavigate();
  const [likes, setLikes] = useState(0);
  const [dislikes, setDislikes] = useState(0);
  const [action, setAction] = useState(null);

  const like = () => {
    setLikes(1);
    setDislikes(0);
    setAction("liked");
  };

  const dislike = () => {
    setLikes(0);
    setDislikes(1);
    setAction("disliked");
  };
  const handleonClick = () => {
    setisClicked(true);
  };
  const actions = [
    <Tooltip key="comment-basic-like" title="Like">
      <span onClick={like}>
        {createElement(action === "liked" ? LikeFilled : LikeOutlined)}
        <span className="comment-action">{likes}</span>
      </span>
    </Tooltip>,
    <Tooltip key="comment-basic-dislike" title="Dislike">
      <span onClick={dislike}>
        {React.createElement(
          action === "disliked" ? DislikeFilled : DislikeOutlined
        )}
        <span className="comment-action">{dislikes}</span>
      </span>
    </Tooltip>,
    <span key="comment-basic-reply-to">Reply to</span>,
  ];

  return (
    <>
      <div>
        <Comment
          actions={actions}
          author={<a>Admin</a>}
          avatar={<Avatar alt="Admin" />}
          content={
            <div dangerouslySetInnerHTML={{ __html: props.content }}></div>
          }
          datetime={
            <Tooltip title={moment().format("YYYY-MM-DD HH:mm:ss")}>
              <span>{moment().fromNow()}</span>
            </Tooltip>
          }
        />
      </div>
    </>
  );
};

export default Posted;
