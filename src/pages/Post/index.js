/* eslint-disable no-undef */
import React, { useEffect, createElement } from "react";
import { useState } from "react";
import { useNavigate, Link } from "react-router-dom";
import {
  DislikeFilled,
  DislikeOutlined,
  LikeFilled,
  LikeOutlined,
} from "@ant-design/icons";
import { Avatar, Comment, Tooltip } from "antd";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Button } from "antd";
import Posted from "./post";
import axios from "axios";

const Post = () => {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [file, setFile] = useState();
  const [content, setContent] = useState();
  const [comments, setComments] = useState();
  const [user, setUser] = useState();
  const [clicked, setisClicked] = useState(false);
  const [img, setImg] = useState();

  const navigate = useNavigate();

  const handleOnChange = (e, editor) => {
    const data = editor.getData();
    setContent(data);
  };
  useEffect(() => {
    axios.get("http://127.0.0.1:4000/api/getpost").then(function (res) {
      setComments(res.data.comments);
      setUser(res.data.user);
    });
  }, [content]);

  // Button дарахад ажиллах хэсэг
  const handleonClick = () => {
    setisClicked(true);
    var postData = {
      name: sessionStorage.getItem("name"),
      comments: content,
    };
    let axiosConfig = {
      headers: {
        "Content-Type": "application/json;charset=UTF-8",
        "Access-Control-Allow-Origin": "*",
      },
    };
    axios
      .post("http://127.0.0.1:4000/api/post", postData, axiosConfig)
      .then(function (res) {
        if (res.status === 200) {
          return toast.success(`投稿完了`, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        }
      });
  };

  return (
    <>
      {" "}
      <div className="">
        <h1>質問と回答</h1>
        <CKEditor editor={ClassicEditor} onChange={handleOnChange} />{" "}
        <div className="grid justify-items-center">
          <Button
            type="danger"
            shape="round"
            className=" mt-[10px] w-[200px]"
            onClick={handleonClick}
          >
            投稿
          </Button>
        </div>
      </div>
      <Posted content={comments} user={user} />
    </>
  );
};

export default Post;
