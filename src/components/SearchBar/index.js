import React from "react"
import { SearchOutlined } from "@ant-design/icons"

const SearchBar = (props) => {
  return (
    <div className="absolute fixed flex items-center w-[500px] border border-gray-400 bg-white z-50 pl-[12px] ml-[200px] mt-[5px]">
      <div className="text-center m-auto h-[30px] w-[30px] text-[14px] font-light text-gray-400 ">
        <SearchOutlined />
      </div>
      <input
        className="flex w-full h-[38px] bg-white py-auto px-[4px] text-[14px] font-light font-light border-none focus:outline-none"
        placeholder={props.placeholder}
      ></input>
    </div>
  )
}

export default SearchBar
