import React from "react";

const MenuButton = () => {
  return (
    <>
      <div>
        <button className="bg-transparent hover:bg-blue-200 text-zinc-700 font-semibold hover:text-white py-2 px-4 border border-blue-500">
          Button
        </button>
      </div>
    </>
  );
};
export default MenuButton;
